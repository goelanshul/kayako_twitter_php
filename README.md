# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository is for fetching tweets for a given keyword and retweet count. 
* 1.0


### Requirements ###
* You'll be needing PHP5 installed in your system
* Though I have used Apache Server for the project, the same can be run with other PHP stacks as well

### How do I get set up? ###

* Signup with apps.twitter.com and get your consumer keys and secret
* Fill them in **utils/keys.php**
* Copy the whole code base in your webserver directory.
* Enable php module in httpd.conf
* Start apache server
* Now the api is live on your machine 


### Who do I talk to? ###
* For any issues/suggestions, feel free to mail me at : 2007.anshul@gmail.com