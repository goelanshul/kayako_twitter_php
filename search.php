<?php
/*
* Created by PhpStorm.
* User: admin
* Date: 8/11/16
*
* @param : reTweetCount - specify a minimum bar for a tweet to be re-tweeted(default=1)
* @param : maxCount - specify a maximum number of results to be returned(default=15,max=100)
* @param : q - query term (default='#custserv')
--------------------------------------------------------------------------------------------------*/

/*******************************************************************
 *  Includes
 ********************************************************************/
// Matt Harris' Twitter OAuth library
//require_once 'utils/tmhOAuth.php';

// include user keys
require_once 'utils/keys.php';

// include fetchUtil for twitter related requests
require_once 'utils/fetchUtil.php';


/*******************************************************************
 *  Defaults
 ********************************************************************/
$maxTweetCount = 15;  //default tweet number = 15
$resultType = 'mixed'; //default to mixed popular and realtime results
$cacheInterval = 90; // default cache interval = 90 seconds
$query = urlencode('#custserv'); // default search query
$reTweetCount = 1; // default minimum retweets required
if (isset($_GET["q"])) {
    $query = urlencode($_GET["q"]);

}
if (isset($_GET["reTweetCount"])) {
    $reTweetCount = $_GET["reTweetCount"];
    if ($reTweetCount < 0) {
        $reTweetCount = 1;
    }

}
if (isset($_GET["maxCount"])) {
    $maxTweetCount = $_GET["maxCount"];
    if ($maxTweetCount > 100 || $maxTweetCount < 0) {
        $maxTweetCount = 15;
    }

}


/********************************************************************
 *  Internal Methods
 ********************************************************************/
function convertStatusToJson($searchResults = array(), $reTweetCount = 1)
{
    $responseArray = array();
    $tweetHashMap = array();
    for ($i = 0; $i < count($searchResults['statuses']); $i++) {
        $obj = $searchResults['statuses'][$i];
        $tweetHash = hash("md5", $obj['text']);

        // Handling case for min retweetcount and to check same tweet is shown only once
        if ($obj['retweet_count'] >= $reTweetCount) {
            if (!isset($tweetHashMap[$tweetHash])) {

                $textArray = array();
                $text = $obj['text'];
                array_push($textArray, $text);
                $tweetHashMap[$tweetHash] = $textArray;

                $response = array(
                    'timeStamp' => $obj['created_at'],
                    'id' => $obj['id'],
                    'text' => $obj['text'],
                    'lang' => $obj['lang'],
                    'retweetCount' => $obj['retweet_count'],
                    'favoriteCount' => $obj['favorite_count']
                );
                array_push($responseArray, $response);
            } else {
                $flag = false;
                foreach ($tweetHashMap[$tweetHash] as $tweetText) {
                    if (strcmp($tweetText, $obj['text']) == 0) {
                        $flag = true;
                        break;
                    }
                    if (!$flag) {
                        $respose = array(
                            'timeStamp' => $obj['created_at'],
                            'id' => $obj['id'],
                            'text' => $obj['text'],
                            'lang' => $obj['lang'],
                            'retweetCount' => $obj['retweet_count'],
                            'favoriteCount' => $obj['favorite_count']
                        );
                        array_push($responseArray, $respose);
                        array_push($tweetHashMap[$tweetHash], $obj['text']);

                    }

                }
            }
        }
    }
    return $responseArray;
}


/*******************************************************************
 *  OAuth
 ********************************************************************/
// Initialize the object for fetchUtil with OAuth Credentials
$fetchUtil = new fetchUtil();


/*******************************************************************
 *  Search Request
 ********************************************************************/
$searchResults = $fetchUtil->searchRequest(array(
    'url' => '1.1/search/tweets',
    'params' => array(
        'include_entities' => true,
        'count' => $maxTweetCount,
        'result_type' => $resultType,
        'q' => $query,
    )
));


// parsing the tweet response to meet our conditions
$responseArray = convertStatusToJson($searchResults, $reTweetCount);

// Returning the json response
header('Content-Type: application/json');
echo json_encode($responseArray);
