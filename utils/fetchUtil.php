<?php

/*
* Created by PhpStorm.
* User: admin
* Date: 8/11/16
* Calls for OAuth and fetches information once user is authenticated
* Also, Can be used to store data for temporary caching(file based currently)
* Fetches Tweets as per parameters
*/


/*******************************************************************
 *  Includes
 ********************************************************************/
// Matt Harris' Twitter OAuth library
// Handles OAuth and curl requests (publicly available)
require_once 'tmhOAuth.php';
require_once 'keys.php';


class fetchUtil
{
    var $tmhOAuth;
    var $interval = 180; // in seconds
    var $cacheNeeded = false; // Default serving form cache is disabled but can be enabled to handle rate limiting by twitter

    /**
     * Creates a new fetchUtil object
     *
     * @param array $keys , the auth keys used to create a new tmhOAuth object
     * @return void
     */
    public function __construct($cache_interval = 180, $isCacheNeeded = false,$keys = array())
    {

        if(count($keys)<4){
            $keys = array(
                'consumer_key' => $GLOBALS['myConsumerKey'],
                'consumer_secret' => $GLOBALS['myConsumerSecret'],
                'user_token' => $GLOBALS['myAccessToken'],
                'user_secret' => $GLOBALS['myAccessTokenSecret'],
            );
        }
        $this->tmhOAuth = new tmhOAuth($keys);

        $this->interval = $cache_interval;

        $this->cacheNeeded = $isCacheNeeded;
    }

    /**
     * Returns an OAuth authorisation request object
     *
     * @return JSON object of thmOAuth->response['response']
     */
    public function authorizeRequest($fromCache = true)
    {
        // request the user information
        $url = $this->tmhOAuth->url('1.1/account/verify_credentials');
        $file = $this->sanitize($url);

        if ($cachedFile = $this->getCachedContents($file)) {
            return json_decode($cachedFile, true);
        } else {
            $code = $this->tmhOAuth->user_request(array(
                'url' => $url
            ));

            // If the request fails, check to see if there's an older cached file we can use
            if ($code <> 200) {
                if ($fromCache && $cachedFile = $this->getCachedContents($file, true)) {
                    return json_decode($cachedFile, true);
                } else {
                    if ($code == 429) {
                        die("Twitter API rate limit Exceeded!!");
                    }
                    die("verify_credentials connection failure");
                }
            }

            // Decode JSON
            $json = $this->tmhOAuth->response['response'];

            if ($this->cache_needed) {
                $this->setCacheContents($file, $json);
            }

            return json_decode($json, true);
        }
    }

    /**
     * Returns response for search request
     *
     * @param $params the parameters for the request
     * @return JSON object of thmOAuth->response['response']
     */
    public function searchRequest($params, $fromCache = false)
    {
        $params['url'] = $this->tmhOAuth->url($params['url']);
        $file = $this->arrayToString($params);

        // Check if cache for same params exists and old values can be used
        if ($fromCache && $cachedFile = $this->getCachedContents($file)) {
            return json_decode($cachedFile, true);
        } else {
            $code = $this->tmhOAuth->user_request($params);

            // If the request fails, check to see if there's an older cached file we can use
            if ($code <> 200) {
                if ($cachedFile = $this->getCachedContents($file, true)) {
                    return json_decode($cachedFile, true);
                } else {
                    die("Error while performing search");
                }
            }

            // Decode JSON
            $json = $this->tmhOAuth->response['response'];


            $this->setCacheContents($file, $json);

            return json_decode($json, true);
        }
    }

    /**
     * Returns a sanitized string, typically for URLs
     * Source: http://stackoverflow.com/a/2668953/551093
     *
     * @param : $string - The string to sanitize
     * @param : $force_lowercase - Force the string to lowercase(default false)
     * @param : $only_alpha - If set to *true*, will remove all non-alphanumeric characters(default false)
     */
    private function sanitize($string, $force_lowercase = true, $only_alpha = false)
    {
        $strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]",
            "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
            "â€”", "â€“", ",", "<", ".", ">", "/", "?");
        $clean = trim(str_replace($strip, "", strip_tags($string)));
        $clean = preg_replace('/\s+/', "-", $clean);
        $clean = ($only_alpha) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean;
        return ($force_lowercase) ?
            (function_exists('mb_strtolower')) ?
                mb_strtolower($clean, 'UTF-8') :
                strtolower($clean) :
            $clean;
    }

    /**
     * Converts a multidimensional array to a string
     *
     * @param $arr the array to be imploded
     * @return string of imploded array
     */
    private function arrayToString($arr)
    {
        $line = array();
        foreach ($arr as $v) {
            $line[] = is_array($v) ? self::arrayToString($v) : $this->sanitize($v);
        }
        return implode($line);
    }


    /**
     * Checks a file to see if a timely cached version exists
     *
     * @param $file the name of the file to be checked
     * @param $ignore_interval false to use the interval, true to ignore the interval
     * @return cached file, or false if file not present
     */
    private function getCachedContents($file, $ignoreInterval = false)
    {
        $cacheFile = dirname(__FILE__) . '/cache/' . $file;
        $modified = @filemtime($cacheFile);
        $now = time();

        // check the cache file
        if (!$modified || (($now - $modified) > $this->interval) && !$ignoreInterval) {
            if (is_writable(dirname($cacheFile)))
                unlink($cacheFile);
            return false;
        } else {
            return getCachedContents($cacheFile);
        }
    }

    /**
     * Stores a cached file
     *
     * @param $file the name of the file to be saved
     * @param $json the data to be stored in the file
     * @return JSON object of thmOAuth->response['response']
     */
    private function setCacheContents($file, $json)
    {
        $cache_file = dirname(__FILE__) . '/cache/' . $file;

        if ($json && is_writable(dirname($cache_file))) {
            $cacheStatic = fopen($cache_file, 'w');
            fwrite($cacheStatic, $json);
            fclose($cacheStatic);
        }
    }
}
